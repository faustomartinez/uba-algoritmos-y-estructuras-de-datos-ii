# Algoritmos y Estructuras de Datos II / Algoritmos y Estructuras de Datos

1er cuatrimestre 2024 \
Universidad de Buenos Aires

## Prácticas

| Nro | Título                                              | Enunciado                                                                                          | Solución                                                                                                      |
|-----|-----------------------------------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1   | Lógica | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica1.pdf)
| 2   | Especificación de problemas |[Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica2.pdf)
| 3   | Verificación de programas | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica3.hs)
| 4   | Especificación de TADs |[Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica4.hs)
| 5   | Diseño |[Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica5.hs)
| 6   | Complejidad | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica6.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica6.py)
| 7   | Diseño: Elección de estructuras | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica7.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/practica7.py)
| 8   | Ordenamiento | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/enunciados/practica8.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/practicas/soluciones/Practica%208/practica8.py)

## Talleres
| Nro | Título                                              | Enunciado                                                                                          | Solución                                                                                                      |
|-----|-----------------------------------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 1   | Introducción a la Programación Imperativa en Java | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/enunciados/taller1.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/taller1/src/main/java/aed/Funciones.java)
| 2   | Debugging y testing | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/enunciados/taller2.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/taller2/src/main/java/aed/Debugging.java)
| 3   | Programación Orientada a Objetos | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/enunciados/taller3.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/taller3/src/main/java/aed)
| 4   | Colecciones e iteradores | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/enunciados/taller4.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/tree/main/talleres/taller4/src/main/java/aed)
| 5   | Conjunto sobre Árbol Binario de Búsqueda | [Enunciado](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/talleres/enunciados/taller5.pdf) | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/tree/main/talleres/taller5/estudiantes/src/main/java/aed)


## Parciales 
| Nro | Título                                              | Solución                                                                                          |
|-----|-----------------------------------------------------|----------------------------------------------------------------------------------------------------|
| 1   | Primer Parcial - 3 de abril de 2024                 | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/parciales/parcial1/AED2_1parcial_03-04-24.pdf)
| 2   | Segundo Parcial - 28 de Junio de 2024               | [Solución](https://gitlab.com/faustomartinez/uba-algoritmos-y-estructuras-de-datos-ii/-/blob/main/parciales/parcial2/AED2_2parcial_03-04-24.pdf)
