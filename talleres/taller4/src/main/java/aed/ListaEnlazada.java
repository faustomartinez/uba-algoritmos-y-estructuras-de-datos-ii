package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    // Completar atributos privados
    private Nodo primero;
    private Nodo ultimo;

    private class Nodo {
        // Completar
        T valor;
        Nodo sig;
        Nodo prev;
        public Nodo(T v){
            valor=v;
        } 
    }

    public ListaEnlazada() {
        this.primero = null;
        this.ultimo = null;
    }

    public int longitud() {
        ListaIterador iterador = new ListaIterador();
        int contador = 0;
        while (iterador.haySiguiente()) {
            iterador.siguiente();
            contador+=1;
        }
        return contador;
    }

    public void agregarAdelante(T elem) {
        // Seteo el nodo
        Nodo nuevo = new Nodo(elem);

        // Fijo cuales son su siguiente y primero
        nuevo.sig = this.primero;
        nuevo.prev = null;

        // Si habia primero, hago que sun prev apunte al nuevo
        if(primero != null){
            primero.prev = nuevo;
        }

        // Declaro que el primero ahora es el nuevo
        primero = nuevo;
    }

    public void agregarAtras(T elem) {
        // Seteo el nodo
        Nodo nuevo = new Nodo(elem);

        // Fijo el siguiente como null
        nuevo.sig = null;

        // Si la lista está vacía, hago que el nuevo sea el primero
        if(primero == null){
            nuevo.prev = null;
            primero = nuevo;       
        // Si no, recorro todo, hasta llegar al ultimo, y fijo que el sig del ultimo sea el nuevo
        // y el prev del nuevo sea el ultimo
        }else{
            Nodo actual = primero;
            while (actual.sig != null) {
                actual = actual.sig;
            }
            actual.sig = nuevo;
            nuevo.prev = actual;
        }


        
    }

    public T obtener(int i) {
        ListaIterador iterador = new ListaIterador();
        // Voy avanzando por todos los elementos, hasta llegar al que quiero, y lo devuelvo
        for(int j=0;j<i;j++){
            iterador.siguiente();
        }
        return iterador.siguiente();
    }


    public void eliminar(int n) {
        ListaIterador iterador = new ListaIterador();
        for(int i = 0; iterador.haySiguiente() && i<n; i++){
            iterador.siguiente();
        }
        // Una vez finalizado el ciclo, tengo en iterador.actual.sig al Nodo que quiero eliminar

        // Si el nodo a ser eliminado es el primero, digo que el nuevo primero será el siguiente al eliminado
        if(primero == iterador.actual.sig){
            primero = iterador.actual.sig.sig;
        }

        // Si el nodo no es el último, fijo que el previo del siguiente sea el de indice dos atras
        // Ej en [1,2,3,4,5] elimino 3, fijo que el prev de 4 sea 2
        if(iterador.actual.sig.sig != null){
            iterador.actual.sig.sig.prev = iterador.actual.sig.prev;
        }

        // Si el nodo no es el primero, fijo que el siguiente del previo sea el de indice dos adelante
        // Ej en [1,2,3,4,5] elimino 3, fijo que el sig de 2 sea 4
        if(iterador.actual.sig.prev != null){
            iterador.actual.sig.prev.sig = iterador.actual.sig.sig;
        }
    }

    public void modificarPosicion(int indice, T elem) {
        ListaIterador iterador = new ListaIterador();
        for (int i = 0; iterador.haySiguiente() && i<indice;i++){
            iterador.siguiente();
        }
        // Ahora, iterador.actual.sig es el nodo a modificar
        iterador.actual.sig.valor = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> nueva = new ListaEnlazada<>();
        for (int j = 0; j<this.longitud(); j++){
            nueva.agregarAtras(this.obtener(j));
        }
        return nueva;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        for(int j = 0;j<lista.longitud();j++){
            this.agregarAtras(lista.obtener(j));
        }
    }
    
    @Override
    public String toString() {
        StringBuffer sbuffer = new StringBuffer();
        ListaIterador iterador = new ListaIterador();
        sbuffer.append('[');
        while (iterador.haySiguiente()) {
            sbuffer.append(iterador.siguiente().toString()+", ");
        }
        sbuffer.delete(sbuffer.length()-2, sbuffer.length());
        sbuffer.append(']');
        return sbuffer.toString();
    }

    private class ListaIterador implements Iterador<T> {
    	// Completar atributos privados
        private Nodo actual = new Nodo(null);

        public ListaIterador(){
            actual.sig = primero;
            actual.prev = null;
        }

        public boolean haySiguiente() {
            return actual.sig != null;
        }
        
        public boolean hayAnterior() {
            return actual.prev != null;
        }

        public T siguiente() {
            actual.prev = actual.sig;
            actual.sig = actual.sig.sig;
            return actual.prev.valor;
        }
         
        public T anterior() {
            actual.sig = actual.prev;
            actual.prev = actual.prev.prev;
            return actual.sig.valor;
        }
    }

    public Iterador<T> iterador() {
	    return new ListaIterador();
    }

}
