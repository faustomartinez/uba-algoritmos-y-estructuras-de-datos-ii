package aed;

class ArregloRedimensionableDeRecordatorios implements SecuenciaDeRecordatorios {

    private Recordatorio[] arregloDeRecordatorios;

    public ArregloRedimensionableDeRecordatorios() {
        this.arregloDeRecordatorios = new Recordatorio[0];
    }

    public ArregloRedimensionableDeRecordatorios(ArregloRedimensionableDeRecordatorios vector) {
        this.arregloDeRecordatorios = vector.copiar().arregloDeRecordatorios;
    }

    public int longitud() {
        return this.arregloDeRecordatorios.length;
    }

    public void agregarAtras(Recordatorio recordatorio) {
        Recordatorio[] nuevoArreglo = new Recordatorio[this.longitud()+1];
        for (int j = 0; j<this.longitud(); j++){
            nuevoArreglo[j] = this.obtener(j);
        } 
        nuevoArreglo[this.longitud()] = recordatorio;
        this.arregloDeRecordatorios = nuevoArreglo;
    }

    public Recordatorio obtener(int i) {
        return arregloDeRecordatorios[i];
    }

    public void quitarAtras() {
        Recordatorio[] nuevoArreglo = new Recordatorio[this.longitud()-1];
        for (int j=0; j<this.longitud()-1;j++){
            nuevoArreglo[j] = this.obtener(j);
        }
        this.arregloDeRecordatorios = nuevoArreglo;
    }

    public void modificarPosicion(int indice, Recordatorio valor) {
        this.arregloDeRecordatorios[indice]=valor;
    }

    public ArregloRedimensionableDeRecordatorios copiar() {
        ArregloRedimensionableDeRecordatorios nuevoArreglo = new ArregloRedimensionableDeRecordatorios();
        for (int j = 0; j<this.longitud(); j++){
            nuevoArreglo.agregarAtras(this.obtener(j));
        }
        return nuevoArreglo;
    }

}
