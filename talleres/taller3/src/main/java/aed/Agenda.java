package aed;

public class Agenda {

    private Fecha fechaActual;
    private ArregloRedimensionableDeRecordatorios recordatorios = new ArregloRedimensionableDeRecordatorios();

    public Agenda(Fecha fechaActual) {
        this.fechaActual = fechaActual;
    }

    public void agregarRecordatorio(Recordatorio recordatorio) {
        this.recordatorios.agregarAtras(recordatorio);
    }

    @Override
    public String toString() {
        StringBuffer sbuffer = new StringBuffer();
        sbuffer.append(this.fechaActual.toString() + "\n");
        sbuffer.append("====="+"\n");
        for (int i=0;i<this.recordatorios.longitud();i++){
            if(this.fechaActual.equals(recordatorios.obtener(i).fecha())){
            sbuffer.append (this.recordatorios.obtener(i).toString()+"\n");
            }
        }
        return sbuffer.toString();      
    }

    public void incrementarDia() {
        this.fechaActual.incrementarDia();
    }

    public Fecha fechaActual() {
        Fecha fecha = new Fecha(this.fechaActual);
        return fecha;
    }

}
