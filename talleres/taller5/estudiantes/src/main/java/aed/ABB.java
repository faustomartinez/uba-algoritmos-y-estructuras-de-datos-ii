package aed;
import java.util.Stack;
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    // Agregar atributos privados del Conjunto
    private Nodo raiz;

    private class Nodo {
        // Agregar atributos privados del Nodo
        T valor;
        Nodo izq;
        Nodo der;

        private Nodo(T v){
            valor = v;
            izq = null;
            der = null;
        }
        
        // Crear Constructor del nodo
    }

    public ABB() {
        raiz=null;
    }

    public ABB(Nodo raizNueva){
        raiz = raizNueva;
    }

    public int cardinal() {
        if(raiz==null){
            return 0;
        }else{
            ABB<T> arbolIzquierdo = new ABB<>(raiz.izq);
            ABB<T> arbolDerecho = new ABB<>(raiz.der);
            return 1+arbolIzquierdo.cardinal()+arbolDerecho.cardinal();
        }
    }

    public T minimo(){
        return minimoHelper(raiz);
    }

    public T minimoHelper(Nodo nodo){
        if(nodo.izq==null){
            return nodo.valor;
        }else{
            return minimoHelper(nodo.izq);
        }
    }

    public T maximo(){
        return maximoHelper(raiz);    
    }

    public T maximoHelper(Nodo nodo){
        if(nodo.der==null){
            return nodo.valor;
        }else{
            return maximoHelper(nodo.der);
        }
    }

    public void insertar(T elem){
        raiz = insertarHelper(raiz,elem);
    }

    public Nodo insertarHelper(Nodo raiz,T elem){
        if(raiz==null){
            raiz = new Nodo(elem);
            return raiz;
        }else if(elem.compareTo(raiz.valor)<0){
            raiz.izq = insertarHelper(raiz.izq, elem);
        }else if(elem.compareTo(raiz.valor)>0){
            raiz.der = insertarHelper(raiz.der,elem);
        }
        return raiz;
    }

    public boolean pertenece(T elem){
        if(raiz==null){
            return false;
        }else if(raiz.valor.compareTo(elem)==0){
            return true;
        }else{
            if(raiz.valor.compareTo(elem)>0){
                ABB<T> arbolIzquierdo = new ABB<>(raiz.izq);
                return arbolIzquierdo.pertenece(elem);
            }else{
                ABB<T> arbolDerecho = new ABB<>(raiz.der);
                return arbolDerecho.pertenece(elem);
            }
        }
    }

    

    public void eliminar(T elem){
        raiz = eliminarHelper(raiz,elem);
    }

    public Nodo eliminarHelper(Nodo raiz,T elem){
        if(raiz == null){
            return raiz;
        }

        if(elem.compareTo(raiz.valor)<0){
            raiz.izq = eliminarHelper(raiz.izq, elem);
        }else if(elem.compareTo(raiz.valor)>0){
            raiz.der = eliminarHelper(raiz.der, elem);
        }else{
            if(raiz.izq==null){
                return raiz.der;
            }else if(raiz.der==null){
                return raiz.izq;
            }else{
                raiz.valor=minimoHelper(raiz.der);
                raiz.der=eliminarHelper(raiz.der, raiz.valor);
            }
        }
        return raiz;
    }



    public String toString(){
        String s = ('{'+enorden(raiz).toString().substring(0, enorden(raiz).toString().length()-1)+'}');
        return s;
    }

    StringBuffer enorden(Nodo raiz) {
        StringBuffer sbuffer = new StringBuffer(); 
        if (raiz != null) {
            sbuffer.append(enorden(raiz.izq).toString()+raiz.valor.toString() + ","+enorden(raiz.der).toString());
        }
        return sbuffer;
    }


    private class ABB_Iterador implements Iterador<T> {
        private Stack<Nodo> stack;

        public ABB_Iterador(){
            stack = new Stack<Nodo>();
            mandarALaIzquierda(raiz);
        }

        public boolean haySiguiente() {            
            return !stack.isEmpty();
        }
    
        public T siguiente() {
            if(!haySiguiente()){
                throw new java.util.EmptyStackException();
            }else{
                Nodo raiz = stack.pop();
                mandarALaIzquierda(raiz.der);
                return raiz.valor;
            }
        }

        private void mandarALaIzquierda(Nodo nodo){
            while(nodo!=null){
                stack.push(nodo);
                nodo = nodo.izq;
            }
        }
    }

    public Nodo minimoFinder(Nodo nodo){
        if(nodo.izq==null){
            return nodo;
        }else{
            return minimoFinder(nodo.izq);
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
